(function () {
	let ws = null;
    var scriptName = "snippet.js"; //name of this script, used to get reference to own tag
    var jQuery; //noconflict reference to jquery
    var jqueryPath = "https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"; 
    var jqueryVersion = "3.4.1";
    var scriptTag; //reference to the html script tag

    var polSrc = "https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/6.26.0/polyfill.min.js";
    var wsSrc = "https://unpkg.com/@adonisjs/websocket-client@1.0.9/dist/Ws.browser.js";


    /******** Get reference to self (scriptTag) *********/
    var allScripts = document.getElementsByTagName('script');
    var targetScripts = [];
    for (var i in allScripts) {
        var name = allScripts[i].src
        if(name && name.indexOf(scriptName) > 0)
            targetScripts.push(allScripts[i]);
    }

    scriptTag = targetScripts[targetScripts.length - 1];

    /******** helper function to load external scripts *********/
    function loadScript(src, onLoad) {
        var script_tag = document.createElement('script');
        script_tag.setAttribute("type", "text/javascript");
        script_tag.setAttribute("src", src);

        if (script_tag.readyState) {
            script_tag.onreadystatechange = function () {
                if (this.readyState == 'complete' || this.readyState == 'loaded') {
                    onLoad();
                }
            };
        } else {
            script_tag.onload = onLoad;
        }
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
    }

    /******** helper function to load external css  *********/
    function loadCss(href) {
        var link_tag = document.createElement('link');
        link_tag.setAttribute("type", "text/css");
        link_tag.setAttribute("rel", "stylesheet");
        link_tag.setAttribute("href", href);
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(link_tag);
    }

    /******** load jquery into 'jQuery' variable then call main ********/
    if (window.jQuery === undefined || window.jQuery.fn.jquery !== jqueryVersion) {
        loadScript(jqueryPath, initjQuery);
    } else {
        initjQuery();
    }


    function initjQuery() {
        jQuery = window.jQuery.noConflict(true);
        // Load all the scripts to run the widget
        loadScript(polSrc, function() {
    		loadScript(wsSrc, function() { 
    			main();
    		});
    	});
    }


    /* Connecting the widget to the socket */
    function startWebsocket () {
		ws = adonis.Ws('ws://127.0.0.1').connect();
	  	ws.on('open', () => {
	    	console.log("Socket Connected!");
	    });

	  	ws.on('error', () => {
	  		console.log("Connection Error!");
	  	})
	}

    /******** starting point for your widget ********/
    function main() {
      //your widget code goes here
		
        jQuery(document).ready(function ($) {
        	
        	startWebsocket();

        	
			//or you could wait until the page is ready
			//example jsonp call
			//var jsonp_url = "www.example.com/jsonpscript.js?callback=?";
			//jQuery.getJSON(jsonp_url, function(result) {
			//	alert("win");
			//});

			//example load css
			//loadCss("http://example.com/widget.css");

			//example script load
			//loadScript("http://example.com/anotherscript.js", function() { /* loaded */ });
        });
		
    }

})();